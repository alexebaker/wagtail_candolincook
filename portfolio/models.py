import re
import requests

from bs4 import BeautifulSoup

from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.search import index


class PortfolioIndexPage(Page):
    subpage_types = ['portfolio.PortfolioPage']

    def get_context(self, request):
        context = super().get_context(request)

        pages = self.get_children().live().order_by('-portfoliopage__pub_date')
        featured_pages = [p for p in pages if p.specific.is_featured]
        pages = [p for p in pages if not p.specific.is_featured]

        other_pages = []
        while len(pages) > 0:
            if len(pages) == 1:
                other_pages.append((pages[0], None))
                pages = pages[1:]
            else:
                other_pages.append((pages[0], pages[1]))
                pages = pages[2:]

        context['featured_pages'] = featured_pages
        context['other_pages'] = other_pages
        return context


class PortfolioPage(Page):
    subpage_types = []

    article_url = models.URLField()
    pub_date = models.DateField("Publish date")
    is_featured = models.BooleanField(default=False)
    image_url = models.URLField(blank=True, null=True)
    summary = RichTextField(blank=True)
    article_title = models.CharField(
        max_length=300,
        null=True,
        blank=True
    )

    content_panels = Page.content_panels + [
        FieldPanel('article_url'),
        FieldPanel('pub_date'),
        FieldPanel('is_featured'),
        FieldPanel('article_title'),
        FieldPanel('image_url'),
        FieldPanel('summary')
    ]

    search_fields = Page.search_fields + [
        index.SearchField('article_url'),
        FieldPanel('article_title'),
        index.SearchField('summary'),
    ]

    def save(self, *args, **kwargs):
        if not self.summary:
            self.summary = self.get_summary()
        if not self.article_title:
            self.article_title = self.get_article_title()
        if not self.image_url:
            self.image_url = self.get_image_url()
        return super().save(*args, **kwargs)

    def get_article_title(self):
        resp = requests.get(self.article_url)
        soup = BeautifulSoup(resp.text, 'html.parser')
        article = soup.find('article')
        if article:
            title = article.find('h1')
            if title:
                return title.text
        return None

    def get_image_url(self):
        resp = requests.get(self.article_url)
        soup = BeautifulSoup(resp.text, 'html.parser')
        article = soup.find('article')
        if article:
            images = list(article.find_all('img'))
            if images:
                return images[0]['src']
        return None

    def get_summary(self):
        resp = requests.get(self.article_url)
        soup = BeautifulSoup(resp.text, 'html.parser')
        article = soup.find('article')
        if article:
            for p in article.find_all('p'):
                words = re.findall(r"\b[a-zA-Z'-]+\b", p.text)
                if len(words) > 10:
                    return p.text
        return None
