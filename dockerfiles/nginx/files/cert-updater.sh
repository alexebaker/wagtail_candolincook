#!/usr/bin/env bash


echo 'Checking for updated certs...'

if [[ -f /config/keys/letsencrypt/fullchain.pem && -s /config/keys/letsencrypt/fullchain.pem && -f /config/keys/letsencrypt/privkey.pem && -s /config/keys/letsencrypt/privkey.pem ]] ; then
    nginx_fullchain_md5=$(md5sum /etc/nginx/certs/fullchain.pem | awk '{print $1}')
    nginx_privkey_md5=$(md5sum /etc/nginx/certs/privkey.pem | awk '{print $1}')

    letsencrypt_fullchain_md5=$(md5sum /config/keys/letsencrypt/fullchain.pem | awk '{print $1}')
    letsencrypt_privkey_md5=$(md5sum /config/keys/letsencrypt/privkey.pem | awk '{print $1}')\

    if [[ "$nginx_fullchain_md5" != "$letsencrypt_fullchain_md5" || "$nginx_privkey_md5" != "$letsencrypt_privkey_md5" ]] ; then
        echo 'Detected new certs, copying them and restarting nginx...'

        cp /config/keys/letsencrypt/fullchain.pem /etc/nginx/certs/fullchain.pem
        cp /config/keys/letsencrypt/privkey.pem /etc/nginx/certs/privkey.pem

        chmod 644 /etc/nginx/certs/fullchain.pem
        chmod 600 /etc/nginx/certs/privkey.pem

        nginx -s reload
    fi
fi

echo 'Checking for updated dhparams...'

if [[ -f /config/nginx/dhparams.pem && -s /config/nginx/dhparams.pem ]] ; then
    nginx_dhparam_md5=$(md5sum /etc/nginx/certs/dhparams.pem | awk '{print $1}')
    letsencrypt_dhparam_md5=$(md5sum /config/nginx/dhparams.pem | awk '{print $1}')

    if [[ "$nginx_dhparam_md5" != "$letsencrypt_dhparam_md5" ]] ; then
        echo 'Detected new dhparams, copying them and restarting nginx...'

        cp /config/nginx/dhparams.pem /etc/nginx/certs/dhparams.pem

        chmod 600 /etc/nginx/certs/dhparams.pem

        nginx -s reload
    fi
fi

exit 0
