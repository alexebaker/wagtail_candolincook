from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel


class HomePage(Page):
    subpage_types = [
        'about.AboutPage',
        'portfolio.PortfolioIndexPage',
        'resume.ResumePage'
    ]

    home_page = models.ForeignKey(
        Page,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    tagline = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )

    content_panels = Page.content_panels + [
        FieldPanel('home_page'),
        FieldPanel('tagline')
    ]

    def get_context(self, request, *args, **kwargs):
        if self.home_page:
            return self.home_page.specific.get_context(request, *args, **kwargs)
        return super().get_context(request, *args, **kwargs)

    def get_template(self, request, *args, **kwargs):
        if self.home_page:
            return self.home_page.specific.get_template(request, *args, **kwargs)
        return super().get_template(request, *args, **kwargs)
