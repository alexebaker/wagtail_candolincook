all: wagtail nginx

.PHONY: wagtail
wagtail: dockerfiles/wagtail/Dockerfile
	docker build --no-cache --force-rm --pull \
		--build-arg PYTHON_VERSION=3.7 \
		-f dockerfiles/wagtail/Dockerfile \
		-t registry.gitlab.com/alexebaker/wagtail_candolincook/wagtail:latest .
ifeq ($(PUSH_IMAGE), true)
	docker push registry.gitlab.com/alexebaker/wagtail_candolincook/wagtail
endif

.PHONY: nginx
nginx: dockerfiles/nginx/Dockerfile
	docker build --no-cache --force-rm --pull \
		--build-arg NGINX_VERSION=mainline \
		-f dockerfiles/nginx/Dockerfile \
		-t registry.gitlab.com/alexebaker/wagtail_candolincook/nginx:latest .
ifeq ($(PUSH_IMAGE), true)
	docker push registry.gitlab.com/alexebaker/wagtail_candolincook/nginx
endif
